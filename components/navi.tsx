import Link from "next/link";
import { FunctionComponent } from "react";

interface NavProps {
  posts: any[];
}

const Navi: FunctionComponent<NavProps> = ({ posts }: any) => {
  return (
    <>
      <ul className="flex mb-10">
        <li>
          <div className="mb-1">static routes</div>
          <Link className="my-btn !bg-rose-500" href="/">
            home
          </Link>
          <Link className="my-btn !bg-rose-500" href="/about">
            about
          </Link>
        </li>
        <li>
          <div className="mb-1">dynamic routes</div>
          {posts.map((post: any, idx: number) => (
            <Link key={idx} className="my-btn" href={"/posts/" + post.title.toLowerCase()}>
              {post.title}
            </Link>
          ))}
        </li>
      </ul>
    </>
  );
};

export default Navi;

import { useTranslation } from "next-i18next";
import Link from "next/link";
import { useRouter } from "next/router";

export default function Layout({ children }: any) {
  const { asPath, pathname, locale } = useRouter();
  const { t, i18n } = useTranslation("common");

  return (
    <div className="bg-gradient-to-tl  from-blue-100 via-blue-200 to-blue-400 p-10 min-h-screen">
      <div className="text-3xl font-semibold mb-7">
        Next.js demo with TailwindCSS & i18n
      </div>
      <div>
        current lang: {locale}
        <div className="bg-green-400 p-2 m-2 inline rounded">
          <Link href="/" locale={locale === "en" ? "de" : "en"}>
            {t("change-locale")}
          </Link>
        </div>
      </div>
      <main className="p-5 rounded-md shadow-lg my-5">{children}</main>
      <footer>
        Copyright © 2022, MyCompany
        <br />
        {t("rights")}
      </footer>
    </div>
  );
}

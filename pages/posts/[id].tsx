import Layout from "../../components/layouts";
import { getProps } from "../../common/helpers";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import Navi from "../../components/navi";
import { useTranslation } from "next-i18next";
import Image from "next/image";

function Blog({ posts, post }: any) {
  const { t, i18n } = useTranslation("common");
  return (
    <Layout>
      <Navi posts={posts}></Navi>
      <div className="bg-blue-200 inline-block px-2 py-1 rounded mb-5">
        This is dynamic {t("h1")}
      </div>
      <div className="flex flex-col items-center bg-white rounded-lg border shadow-md md:flex-row md:max-w-xl">
        <Image
          className="object-cover w-full h-96 rounded-t-lg md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
          width="200"
          height="200"
          layout="intrinsic"
          src={post?.imgSrc}
          alt="image"
        />
        <div className="flex flex-col justify-between p-4 leading-normal">
          <h5 className="mb-2 text-2xl font-bold text-gray-900 ">
            {post?.title}
          </h5>
          <h5 className="mb-2 text-gray-500 ">
            price: {post?.price}
          </h5>
          <p className="mb-3 font-normal text-gray-700">
            Here are the biggest enterprise technology acquisitions of 2021 so
            far, in reverse chronological order.
          </p>
        </div>
      </div>
    </Layout>
  );
}

export async function getStaticProps({ locale, params: { id } }: any) {
  const posts: any[] = await getProps();
  const post = posts.find(({ title }: any) => id === title.toLowerCase());

  return {
    props: {
      posts,
      post,
      id,
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export async function getStaticPaths({ locales }: any) {
  const posts: any[] = await getProps();
  const pages = posts.map((post: any) => ({
    params: { id: post.title.toLowerCase() },
  }));

  const paths = locales
    .map((locale: any) => pages.map((ii: any) => ({ ...ii, locale })))
    .flat();

  return {
    paths,
    fallback: false,
  };
}

export default Blog;

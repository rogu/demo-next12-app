import type { NextPage } from "next";
import { useTranslation } from "next-i18next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";
import { getProps } from "../common/helpers";
import Layout from "../components/layouts";
import Navi from "../components/navi";

const Home: NextPage = ({ posts }: any) => {
  const { t, i18n } = useTranslation("common");
  return (
    <Layout>
      <Navi posts={posts}></Navi>
      <div className="bg-blue-200 inline-block px-2 py-1 rounded mb-5">
        This is static {t("h1")}
      </div>
      <div className="p-5 mt-5 text-4xl font-semibold">Home Page</div>
    </Layout>
  );
};

export async function getStaticProps({ locale }: any) {
  const posts: any[] = await getProps();

  return {
    props: {
      posts,
      ...(await serverSideTranslations(locale, ["common"])),
    },
  };
}

export default Home;

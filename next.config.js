/** @type {import('next').NextConfig} */

const { i18n } = require("./next-i18next.config");

const nextConfig = {
  reactStrictMode: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'api.debugger.pl',
        port: '',
        pathname: '/assets/**',
      },
    ],
  },
  i18n
};

module.exports = nextConfig

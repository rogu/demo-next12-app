import { NextResponse, NextRequest, NextFetchEvent } from 'next/server'
export async function middleware(req: NextRequest, ev: NextFetchEvent) {
    const { pathname } = req.nextUrl
    return pathname == '/debugger'
        ? NextResponse.redirect('https://debugger.pl')
        : NextResponse.next();
}
